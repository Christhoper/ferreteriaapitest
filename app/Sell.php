<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    //una venta tiene muchos productos
    public function products(){
        return $this->belongsToMany(Product::class, 'product_sells')->withPivot(['quantity', 'price'])->withTimeStamps();
    }


}
