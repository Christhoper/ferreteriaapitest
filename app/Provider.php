<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{

    //nos referimos a que name es un dato rellanable
    protected $fillable = ['name'];

    //tiene muchos productos
    public function productos(){
        return $this->hasMany(Product::class);
    }

}
