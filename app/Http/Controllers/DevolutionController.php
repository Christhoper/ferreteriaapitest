<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Sell;
use App\Product;

class DevolutionController extends Controller
{

    private $sell;
    private $petition;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      

        //valida que el array no venga vacio
        //abort_unless(blank($request->input('products')));

        //todo dentro de transacción en caso de fallo para no perder stock sin realizar operación
        $this->petition = $request;

        //Validamos que no existan campos nulos

        DB::transaction(function () {
   
            $request = $this->petition;
            
            $sell = new Sell;
        
            //guardamos la venta para poder tomar su id
            $sell->total = 0;
            $sell->save();
            
            //transforma el json en un array asociativo 
            $products = $request->input('products');
    
            $total = 0;
    
            foreach ($products as $product ) {
            //tomamos el objeto venta
            $sell->products()->attach($product['product_id'], ['quantity'=> $product['quantity'] , 'price'=> $product['price']]);
              
            //calculo del total
            $total -= $product['quantity'] * $product['price'];
            
    
            }
    
            
            foreach ($products as $item ) {
                //descuento de stock por cada producto
                $product = Product::find($item['product_id']);
    
                $product->stock += $item['quantity'];
    
                $product->save();
                $product= null;
    
            }
             
            $sell->total = $total;
            $sell->save();    
                 
            $this->sell = $sell; 
    
        });

        return response()->json($this->sell, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
