<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Product;
use App\Provider;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
         //llamar a todos los products
         return Product::all()->load(['provider']);
    }

    public function searchcode($code)
    {
        $product = Product::where('code',$code)->first();//first trae el primer objeto que cumple con la condición
        if($product){
            return $product;
        }else{
            return response()->json("Producto no encontrado", 400);
        }
       
    }

    public function searchname($name)
    {
        //return Product::all()->where('name','like','%'$name.'%');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedor = Proveedor::All();
        return view();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Instanciamos la clase product
        $product = new Product;

        //Declaramos el nombre con el nombre enviado en el request
        $product->provider_id=$request->provider;
        $product->name=$request->name;
        $product->code=$request->code;
        $product->stock=$request->stock;
        $product->cost=$request->cost;
        $product->price=$request->price;
        $product->critical=$request->critical; 

        //Guardamos el cambio en el modelo
        $product->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Solicitamos al modelo el Producto con el id solicitado por get
        return Product::where('id', $id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $product = Product::find($id);

        $product->provider_id=$request->provider;
        $product->name=$request->name;
        $product->code=$request->code;
        $product->stock=$request->stock;
        $product->cost=$request->cost;
        $product->price=$request->price;
        $product->critical=$request->critical;

        $product->save();

        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
    }




}
