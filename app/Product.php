<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //nos referimos a que name es un dato rellanable
    //protected $fillable = [ 'provider_id', 'name','code', 'stock', 'cost', 'price', 'critical'];
    
    //pertenece a un proveedor
    public function provider(){
        return $this->belongsTo(Provider::class);
    }

    //un producto pertenece a muchas ventas
    public function sells(){
        return $this->belongsToMany(Sell::class);
    }




    // Las siguientes líneas de codigo permiten modularizar las búsquedas por nombre y código de barras
    //ver acá https://www.youtube.com/watch?v=bmtD9GUaszw&list=PLhCiuvlix-rQFUWALRuG_EXaslHfwgfck&index=6

  /*  public function scopeCode($query, $code){
        if($code){
            return $query->where('code', 'LIKE', "%$code%");
        }
    }

    public function scopeName($query, $name){
        if($name){
            return $query->where('name', 'LIKE', "%$name%");
        }
    } */

}
