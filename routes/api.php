<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('provider', 'ProviderController');

Route::resource('product', 'ProductController');

Route::resource('sell', 'SellController');

Route::resource('devolution', 'DevolutionController');

Route::resource('credit', 'CreditController');

Route::resource('payment', 'PaymentController');

Route::resource('daily', 'DailyController');

Route::resource('critical', 'CriticalController');

Route::resource('products', 'ProductBySellController');

Route::get('searchname/{name}', 'ProductController@searchName');

Route::get('searchcode/{code}', 'ProductController@searchCode');





//Route::resource('provider/products', 'ProviderController@products');
